<!DOCTYPE  html>
<html lang="en-US">
<head>
    <title>LoboPlan - Subject Select</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/> 
</head>
<body>
    <?php 
    //Grab $_GET values
    $xml_file = $_GET['file'];
    $campus_code = $_GET['campuscode'];
    //create xml document
    $xmlDoc = simplexml_load_file("xml/{$xml_file}");
    $sem = $xmlDoc->xpath("//semester");
    $sem_attrs = $sem[0]->attributes();
    $semester = $sem_attrs['name'];
    $subjects = $xmlDoc->xpath("//campus[@code='{$campus_code}']/college/department/subject");
    //Deconstruction
    $xmlDoc = null;
    $sem = null;
    $sem_attrs = null;
    //Print breadcrumb header
    echo "<div class='header'><a class='breadcrumb'href='index.php?'>LoboPlan</a>/<a class='breadcrumb' href='campus_select.php?semester={$semester}&file={$xml_file}'>{$semester}</a>/<span>{$campus_code}</span></div>\r\n";
    //require "alphabet_header.php";
    if (count($subjects) == 0) {
        echo "<div class='no_results'>Sorry, there is no data in here yet. Check back later!</div>\r\n";
    } else {
        echo "<ul>\r\n";
        $subject_array = array('code'=>'name');
        foreach ($subjects as $subject){
            $subject_attr = $subject->attributes();
            $code = $subject_attr['code'];
            $name = $subject_attr['name'];
            $subject_array[(string)$code] = (string)$name;
            //Decontruction
            $subject_attr = null;
            $code = null;
            $name = null;
        }
        //Sort subject_array alphabetically
        unset($subject_array['code']);
        ksort($subject_array);
        //Print <li> of courses
        foreach($subject_array as $code => $name) {
            echo "<li class='btn'>";
            echo "<a name='".strtoupper(substr($code, 0, 1))."'/>";
            echo "<a href='course_select.php?file={$xml_file}&campuscode={$campus_code}&subcode={$code}'><button>{$code} - {$name}</button></a></li>\r\n";
            $code = null;
            $name = null;
        }
        echo "</ul>\r\n";
        //Deconstruction
        $subject_array = null;
    }
    $xml_file = null;
    $campus_code = null;
    $semester = null;
    $subjects = null;
    require "footer.php";
    ?>
</body>
</html>
