<!DOCTYPE  html>
<html lang="en-US">
<head>
    <title>LoboPlan - Course Select</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/> 
</head>
<body>
    <?php 
    //Grab $_GET values
    $xml_file = $_GET['file'];
    $campus_code = $_GET['campuscode'];
    $subject_code = $_GET['subcode'];
    //Create xml doc
    $xmlDoc = simplexml_load_file("xml/{$xml_file}");
    $sem = $xmlDoc->xpath("//semester");
    $sem_attrs = $sem[0]->attributes();
    $semester = $sem_attrs['name'];
    $courses = $xmlDoc->xpath("//campus[@code='{$campus_code}']/college/department/subject[@code='{$subject_code}']/course");
    //Deconstruction
    $xmlDoc = null;
    $sem = null;
    $sem_attrs = null;
    //Print breadcrumb header
    echo "<div class='header'><a class='breadcrumb' href='index.php'>LoboPlan</a>/<a class='breadcrumb' href='campus_select.php?semester={$semester}&file={$xml_file}'>{$semester}</a>/<a class='breadcrumb' href='subject_select.php?semester={$semester}&file={$xml_file}&campuscode={$campus_code}&subcode={$subject_code}'>{$campus_code}</a>/<span>{$subject_code}</span></div>\r\n";
    if (count($courses) == 0) {
        echo "<div class='no_results'>Sorry, there is no data in here yet. Check back later!</div>\r\n";
    } else {
        echo "<ul>\r\n";
        $course_array = array('code'=>'name');
        foreach ($courses as $course){
            $course_attr = $course->attributes();
            $code = $course_attr['number'];
            $name = $course_attr['title'];
            $course_array[(string)$code] = (string)$name;
            //Deconstruction
            $course_attr = null;
            $code = null;
            $name = null;
        }
        //sort array by code
        unset($course_array['code']);
        ksort($course_array);
        //print each course
        foreach($course_array as $code => $name) {
            echo "<li class='btn'><a href='display.php?file={$xml_file}&campuscode={$campus_code}&subcode={$subject_code}&coursenumber={$code}'><button>{$code} - {$name}</button></a></li>\r\n";
            $code = null;
            $name = null;
        }
        echo "</ul>\r\n";
        $course_array = null;
    }
    //Deconstruction
    $xml_file = null;
    $campus_code = null;
    $subject_code = null;
    $semester = null;
    $courses = null;
    require "footer.php";
    ?>
</body>
</html>
