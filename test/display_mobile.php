<?php
    //Display results
    if (count($results) == 0) {
        echo "<div class='no_results'>Sorry, no results found. Try a different search and/or check your search categories.</div>";
    } else {
        //print results (custom) 
        echo "<div class='results_output'>";
        print_results($results);
        echo "</div>";
        //print results (as an array)
        //echo "<pre><b>Full Output of Courses</b><br>";
        //print_r($results);
        //echo "</pre>";
    }

    //functions for setup.php and parseXML.php to use
    function print_results($results) {
        //start designing the table (headings) in order to organize output
        //echo "<table><tr><th>";
        foreach ($results as $courseXmlElement) {
            //echo "<b>Course Details:</b><br>";
            $course_attr = $courseXmlElement->attributes();
            echo "<div class='results_table_header'>{$_POST['subjectCode']} {$course_attr['number']}: {$course_attr['title']}";
            //foreach ($course_attr as $attr) {
            //    echo "{$attr->getName()} = {$attr}<br>";
            //}
            $course_children = $courseXmlElement->children();
            foreach ($course_children as $course_child) {
                $course_child_name = $course_child->getName();
                if ($course_child_name != 'section') {
                    echo "<div class='results_table_header_sub'><b>Catalog Description:</b> {$course_child}</div></div>";
                } else {
                    echo "<table><tr><th>CRN</th><th>Section Number</th><th>Term</th><th>Status</th><th>Delivery</th><th>Enrollment/Max</th><th>Waitlist/Max</th><th>Credits</th><th>Fees</th></tr><tr>";
                    foreach ($course_children as $section) {
                        if (count($section->children()) > 0) {
                            //Attributes of each section
                            $section_attr = $section->attributes();
                            foreach ($section_attr as $attr) {
                                echo "<td>{$attr}</td>";
                            }
                            //section Children
                            $section_children = $section->children();
                            foreach ($section_children as $node) {
                                $node_name = $node->getName();
                                $node_attr = $node->attributes();
                                /*
                                if ($node_name == 'instructors') {
                                    //section instructors
                                    echo "<b>Instructor Details:</b><br>";
                                    foreach ($node as $instructor) {
                                        if ($instructor->attributes()['primary'] == 'y') {
                                            $inst_children = $instructor->children();
                                            foreach ($inst_children as $child) {
                                                $node_name = $child->getName();
                                                echo "{$node_name} = {$child}<br>";
                                            }
                                        }
                                    } 
                                } else if ($node_name == 'meeting-times') {
                                    //meeting-times stuff
                                    //section meeting-times
                                    echo "<b>Meeting Times Details</b><br>";
                                    //$meeting_times = $section_children['meeting-times'];
                                    //if ($meeting_times) { echo "meeting_times is something"; }
                                    foreach ($node as $meeting_time) {
                                        $meeting_time_children = $meeting_time->children();
                                        foreach ($meeting_time_children as $meeting_child) {
                                            $meeting_child_name = $meeting_child->getName();
                                            if ($meeting_child_name != 'days') {
                                                echo "{$meeting_child_name} = {$meeting_child}<br>";
                                            } else {
                                                $days = $meeting_child->children();
                                                echo "Meeting days = ";
                                                foreach ($days as $day) {
                                                    echo "{$day}";
                                                }
                                                echo "<br>";
                                            }
                                        }
                                    }
                                } else */if ($node_name != 'crosslists' and $node_name != 'section-title' and $node_name != 'text' and $node_name != 'instructional-method' and $node_name != 'instructors' and $node_name != 'meeting-times') {
                                    if (count($node_attr) > 0) {
                                        echo "<td>{$node}/{$node_attr[0]}</td>";
                                    } else {
                                        echo "<td>{$node}</td>";
                                    }
                                }
                            }
                            echo "</tr>";
                        }
                    }
                    //end of foreach of sections
                    echo "</table><br>";
                }  
            }
        }
    }
?>
