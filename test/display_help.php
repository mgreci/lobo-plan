<?php
    //grab parameters from $_GET
    //semester
    $semester = $_GET['semester'].".xml";
    //campus
    $campus = "campus";
    if (strcmp($_GET['campus'],"*")) { 
        $campus = $campus."[@code='".$_GET['campus']."'";
        if (strcmp($_GET['includeOnline'], '')) {
            $campus .= " or @code='EA'";
        }
        $campus .= "]";
    }
    //subjectCode
    $subjectCode = "subject";
    if (strcmp($_GET['subjectCode'],"*")) { 
        $subjectCode = $subjectCode."[@code='".$_GET['subjectCode']."']";
    }
    //courseNumber
    $courseNumber = "course";
    if ($_GET['courseNumber']) { 
        $courseNumber = $courseNumber."[@number='".$_GET['courseNumber']."']";
    }     
    //status of course
    $sectionStatus = "section";
    if ($_GET['activeSectionOnly'] == 'T') {
        $sectionStatus .= "[@status='A']";
    } else {
        echo "section status not T, not searching for Active";
    }
    //Setup SimpleXML
    $xml = simplexml_load_file("xml/{$semester}") or die("Error: cannot create object");
    //Begin search
    $results = $xml->xpath("//semester/{$campus}/college/department/{$subjectCode}/{$courseNumber}[{$sectionStatus}]");
?>
