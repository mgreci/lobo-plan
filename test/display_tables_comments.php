<?php
    //Display results
    if (count($results) == 0) {
        echo "<div class='no_results'>Sorry, no results found. Try a different search and/or check your search categories.</div>";
    } else {
        //print results (custom) 
        echo "<div class='results_output'>";
        print_results($results);
        echo "</div>";
        //print results (as an array)
        //echo "<pre><b>Full Output of Courses</b><br>";
        //print_r($results);
        //echo "</pre>";
    }

    //functions for setup.php and parseXML.php to use
    function print_results($results) {
        //start designing the table (headings) in order to organize output
        //echo "<table><tr><th>";
        foreach ($results as $courseXmlElement) {
            //echo "<b>Course Details:</b><br>";
            $course_attr = $courseXmlElement->attributes();
            echo "<div class='results_table_header'>{$_POST['subjectCode']} {$course_attr['number']}: {$course_attr['title']}";
            //foreach ($course_attr as $attr) {
            //    echo "{$attr->getName()} = {$attr}<br>";
            //}
            $course_children = $courseXmlElement->children();
            foreach ($course_children as $course_child) {
                $course_child_name = $course_child->getName();
                if ($course_child_name == 'catalog-description') {
                    echo "<div class='results_table_header_sub'><b>Catalog Description:</b> {$course_child}</div></div>";
                } else { //otherwise the child is section
                    echo "<table><tr><th>CRN</th><th>Section Number</th><th>Term</th><th>Status</th><th>Instructor</th><th>Meeting Time</th><th>ENR/Max</th><th>WL/Max</th><th>Credits</th><th>Fees</th></tr><tr>";
                    foreach ($course_children as $section) {
                        if (count($section->children()) > 0) { //section has children/nonempty
                            //Attributes of each section
                            $section_attr = $section->attributes();
                            //sections had 4 attributes and should always be in same order
                            if (count($section_attr) == 4) {
                                $crn = $section_attr[0];
                                $sectionNumber = $section_attr[1];
                                $part_of_term = $section_attr[2];
                                $sectionStatus = $section_attr[3];
                            } else { //section doesnt have 4 attributes
                                echo "Warning: section does not have 4 attributes";
                            }
                            /*
                            //foreach will not work as we can not customize the table layout
                            foreach ($section_attr as $attr) {
                                echo "<td>{$attr}</td>";
                            }
                            */
                            //section Children
                            $section_children = $section->children();
                            foreach ($section_children as $node) {
                                $node_name = $node->getName();
                                $node_attr = $node->attributes();
                                //instructors
                                if ($node_name == 'instructors') {
                                    //section instructors
                                    foreach ($node as $instructor) {
                                        if ($instructor->attributes()['primary'] == 'y') {
                                            $inst_children = $instructor->children();
                                            $inst_firstname = $inst_children[0];
                                            $inst_lastname = $inst_children[1];
                                            $inst_middle = $inst_children[2];
                                            $inst_full = $inst_lastname.', '.$inst_firstname.' '.$inst_middle;
                                            //$inst_email = inst_children[3];
                                            // "lastname, firstname middle"
                                            // "Greci, Matthew J"
                                            //echo "<td>{$inst_lastname},{$inst_firstname} {$inst_middle}</td>";
                                            //foreach cant create custom table
                                            /*
                                            foreach ($inst_children as $child) {
                                            //    $node_name = $child->getName();
                                            //    echo "{$node_name} = {$child}<br>";
                                            //}
                                            */
                                        }
                                    } 
                                } else if ($node_name == 'meeting-times') {
                                    //meeting-times stuff
                                    //section meeting-times
                                    //echo "<b>Meeting Times Details</b><br>";
                                    //$meeting_times = $section_children['meeting-times'];
                                    //if ($meeting_times) { echo "meeting_times is something"; }
                                    $meeting_final = "";
                                    foreach ($node as $meeting_time) {
                                        $meeting_time_children = $meeting_time->children();
                                        $start_date = $meeting_time_children[0];
                                        $end_date = $meeting_time_children[1];
                                        $days = $meeting_time_children[2]->children();
                                        $days_output = "";
                                        foreach ($days as $day) {
                                            $days_output .= $day;
                                        }
                                        $start_time = $meeting_time_children[3];
                                        $end_time = $meeting_time_children[4];
                                        $bldg_array = $meeting_time_children[5];
                                        $bldg_code = $bldg_array->attributes()['code'];
                                        $room = $meeting_time_children[6];
                                        $meeting_time_output = $days_output.' '.$start_time.'-'.$end_time;
                                        $meeting_time_location = $bldg_code.' '.$room;
                                        $meeting_final .= '<br>'.$meeting_time_output;
                                        /*
                                        foreach ($meeting_time_children as $meeting_child) {
                                            $meeting_child_name = $meeting_child->getName();
                                            if ($meeting_child_name != 'days') {
                                                echo "{$meeting_child_name} = {$meeting_child}<br>";
                                            } else {
                                                $days = $meeting_child->children();
                                                echo "Meeting days = ";
                                                foreach ($days as $day) {
                                                    echo "{$day}";
                                                }
                                                echo "<br>";
                                            }
                                        }
                                        */
                                    }
                                } else if ($node_name == 'enrollment') {
                                    $enrollment = $node;
                                    $enrollment_max = $node->attributes()['max'];
                                    $enrollment_full = $enrollment.'/'.$enrollment_max;
                                } else if ($node_name == 'waitlist') {
                                    $waitlist = $node;
                                    $waitlist_max = $node->attributes()['max'];
                                    $waitlist_full = $waitlist.'/'.$waitlist_max;
                                } else if ($node_name == 'credits') {
                                    $credits = $node;
                                } else if ($node_name == 'fees') {
                                    $fees = $node;
                                }
                            }
                            //end of 'row' or section
                            //here we will build the table/output
                            echo "<td>{$crn}</td><td>{$sectionNumber}</td><td>{$part_of_term}</td><td>{$sectionStatus}</td><td>{$inst_full}</td><td>{$meeting_final}</td><td>{$enrollment_full}</td><td>{$waitlist_full}</td><td>{$credits}</td><td>{$fees}</td></tr>";
                        }
                    }
                    //end of foreach of sections
                    echo "</table><br>";
                }  
            }
        }
    }
?>
