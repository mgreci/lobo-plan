<?php
$file = $_GET['file'];
$campus_code = $_GET['campus_code'];
$subject_code = $_GET['subject_code'];
$course_number = $_GET['course_number'];
$instructor_last = $_GET['instructor_last'];
$meet_days = $_GET['meet_days'];
$start_after = $_GET['start_after'];
$start_before = $_GET['start_before'];
$end_before = $_GET['end_before'];
$end_after = $_GET['end_after'];
$crn = $_GET['crn'];
$include_online = $_GET['include_online'];
$show_cancelled = $_GET['show_cancelled'];

echo "file = {$file}<br>campus_code = {$campus_code}<br>subject_code = {$subject_code}<br>course_number = {$course_number}<br>instructor last name = {$instructor_last}<br>meet_days = {$meet_days}<br>start after = {$start_after}<br>start before = {$start_before}<br>end before = {$end_before}<br>end after = {$end_after}<br>crn = {$crn}<br>include online = {$include_online}<br>show cancelled = {$show_cancelled}<br>";

//Generate $results array, this uses variables stored in $_GET
//run through $results and print sections
//$results should return an array with one course.
//We can then print all the information we need from just that

//Generate $results
//$xml = simplexml_load_file("xml/{$file}") or die("Error: cannot create object");
//$results = $xml->xpath("//{$campus}/college/department/{$subject}/{$course}");
//results is now an array with one course item.

/*
   $course = $results[0];
   $course_children = $course->children();
   $catalog_desciption = $course_children[0];
   foreach ($course_children as $child) {
   if ($child->getName() != 'catalog-description') {
//$child is now a section
}
}
 */
/*
//Display results
if (count($results) == 0) {
    echo "<div class='no_results'>Sorry, no results found. Try a different search and/or check your search categories.</div>";
} else {
    //print results (custom) 
    echo "<div class='results_output'>";
    print_results($results);
    echo "</div>";
    //print results (as an array)
    //echo "<pre><b>Full Output of Courses</b><br>";
    //print_r($results);
    //echo "</pre>";
}

function print_results($results) {
    //start designing the table (headings) in order to organize output
    foreach ($results as $courseXmlElement) {
        //print header for each course (ECE 101, ECE 213)
        $course_attr = $courseXmlElement->attributes();
        echo "<div class='results_table_header'>{$_POST['subjectCode']} {$course_attr['number']}: {$course_attr['title']}";
        //print catalog_des below header "This course..."
        $course_children = $courseXmlElement->children();
        $catalog_des = $course_children[0];
        echo "<div class='results_table_header_sub'><b>Catalog Description:</b> {$catalog_des}</div></div>";
        //Create table and table headers
        echo "<table><tr><th>CRN</th><th>Section Number</th><th>Term</th><th>Status</th><th>Instructor</th><th>Meeting Time</th><th>ENR/Max</th><th>WL/Max</th><th>Credits</th><th>Fees</th></tr><tr>";
        foreach ($course_children as $section) {
            if (count($section->children()) > 0) { //section has children/nonempty
                //Attributes of each section
                /*
                $section_attr = $section->attributes();
                //sections had 4 attributes and should always be in same order
                if (count($section_attr) == 4) {
                    $crn = $section_attr[0];
                    $sectionNumber = $section_attr[1];
                    $part_of_term = $section_attr[2];
                    $sectionStatus = $section_attr[3];
                    if ($sectionStatus != 'A') {
                        echo "Section status (not A): {$sectionStatus}<br>";
                    }
                } else { //section doesnt have 4 attributes
                    //this output is just for debugging
                    echo "Warning: section does not have 4 attributes";
                }
                //section Children
                $section_children = $section->children();
                foreach ($section_children as $node) {
                    $node_name = $node->getName();
                    $node_attr = $node->attributes();
                    //instructors
                    if ($node_name == 'instructors') {
                        //section instructors
                        foreach ($node as $instructor) {
                            if ($instructor->attributes()['primary'] == 'y') {
                                $inst_children = $instructor->children();
                                $inst_firstname = $inst_children[0];
                                $inst_lastname = $inst_children[1];
                                $inst_middle = $inst_children[2];
                                $inst_full = $inst_lastname.', '.$inst_firstname.' '.$inst_middle;
                            }
                        } 
                    } else if ($node_name == 'meeting-times') {
                        //meeting-times stuff
                        $meeting_final = "";
                        foreach ($node as $meeting_time) {
                            $meeting_time_children = $meeting_time->children();
                            $start_date = $meeting_time_children[0];
                            $end_date = $meeting_time_children[1];
                            $days = $meeting_time_children[2]->children();
                            $days_output = "";
                            foreach ($days as $day) {
                                $days_output .= $day;
                            }
                            $start_time = $meeting_time_children[3];
                            $end_time = $meeting_time_children[4];
                            $bldg_array = $meeting_time_children[5];
                            $bldg_code = $bldg_array->attributes()['code'];
                            $room = $meeting_time_children[6];
                            $meeting_time_output = $days_output.' '.$start_time.'-'.$end_time;
                            $meeting_time_location = $bldg_code.' '.$room;
                            $meeting_final .= $meeting_time_output.'<br>';
                        }
                    } else if ($node_name == 'enrollment') {
                        $enrollment = $node;
                        $enrollment_max = $node->attributes()['max'];
                        $enrollment_full = $enrollment.'/'.$enrollment_max;
                    } else if ($node_name == 'waitlist') {
                        $waitlist = $node;
                        $waitlist_max = $node->attributes()['max'];
                        $waitlist_full = $waitlist.'/'.$waitlist_max;
                    } else if ($node_name == 'credits') {
                        $credits = $node;
                    } else if ($node_name == 'fees') {
                        $fees = $node;
                    }
                }
                //end of 'row' or section
                //here we will build the table/output
                echo "<td>{$crn}</td><td>{$sectionNumber}</td><td>{$part_of_term}</td><td>{$sectionStatus}</td><td>{$inst_full}</td><td>{$meeting_final}</td><td>{$enrollment_full}</td><td>{$waitlist_full}</td><td>{$credits}</td><td>{$fees}</td></tr>";
            }
        }
        //end of foreach of sections
        echo "</table><br>";
    }
    //end of results array
}
//end of function
*/
?>
