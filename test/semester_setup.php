<?php
    //Create SimpleXML objects
    $xml_path = "xml/";
    //Find names of semseters
    $xml = simplexml_load_file("{$xml_path}current.xml") or die("Error: cannot create object");
    $sem_current = $xml->xpath("//semester");
    $sem_name = $sem_current[0]->attributes()['name'];
    echo "<li><a href='campus_select.php?semester={$sem_name}&file=current.xml' class='' title='{$sem_name}'>{$sem_name}</a></li>";
    $xml = simplexml_load_file("{$xml_path}next1.xml") or die("Error: cannot create object");
    $sem_next1 = $xml->xpath("//semester");
    $sem_name = $sem_next1[0]->attributes()['name'];
    echo "<li><a href='campus_select.php?semester={$sem_name}&file=next1.xml' class='' title='{$sem_name}'>{$sem_name}</a></li>";
    $xml = simplexml_load_file("{$xml_path}next2.xml") or die("Error: cannot create object");
    $sem_next2 = $xml->xpath("//semester");
    $sem_name = $sem_next2[0]->attributes()['name'];
    echo "<li><a href='campus_select.php?semester={$sem_name}&file=next2.xml' class='' title='{$sem_name}'>{$sem_name}</a></li>";
    $xml = simplexml_load_file("{$xml_path}next3.xml") or die("Error: cannot create object");
    $sem_next3 = $xml->xpath("//semester");
    $sem_name = $sem_next3[0]->attributes()['name'];
    echo "<li><a href='campus_select.php?semester={$sem_name}&file=next3.xml' class='' title='{$sem_name}'>{$sem_name}</a></li>";
    $xml = null;
    /*
    Expected Output:
    <li><a href='campus_select.php?semester=Fall 2014&file=current.xml' class='' title='Fall 2014'>Fall 2014</a></li>
    <li><a href='campus_select.php?semester=Spring 2015&file=next1.xml' class='' title='Spring 2015'>Spring 2015</a></li>
    <li><a href='campus_select.php?semester=Summer 2015&file=next2.xml' class='' title='Summer 2015'>Summer 2015</a></li>
    <li><a href='campus_select.php?semester=Fall 2015&file=next3.xml' class='' title='Fall 2015'>Fall 2015</a></li>
    */
?>
