<?php
    //Create SimpleXML objects
    $xml_path = "../xml/";
    $simple_current = simplexml_load_file("{$xml_path}current.xml") or die("Error: cannot create object");
    $simple_next1 = simplexml_load_file("{$xml_path}next1.xml") or die("Error: cannot create object");
     
    //Find names of semseters
    $xml = $simple_current;
    $sem_current = $xml->xpath("//semester");
    $sem_name = $sem_current[0]->attributes()['name'];
    echo "<option value='current.xml' selected>{$sem_name}</option>";
    $xml = $simple_next1;
    $sem_next1 = $xml->xpath("//semester");
    $sem_name = $sem_next1[0]->attributes()['name'];
    echo "<option value='next1.xml'>{$sem_name}</option>";
    $xml = simplexml_load_file("{$xml_path}next2.xml") or die("Error: cannot create object");
    $sem_next2 = $xml->xpath("//semester");
    $sem_name = $sem_next2[0]->attributes()['name'];
    echo "<option value='next2.xml'>{$sem_name}</option>";
    $xml = simplexml_load_file("{$xml_path}next3.xml") or die("Error: cannot create object");
    $sem_next3 = $xml->xpath("//semester");
    $sem_name = $sem_next3[0]->attributes()['name'];
    echo "<option value='next3.xml'>{$sem_name}</option>";
    /*
    Output should be
    <option value="current" selected>Fall 2014</option>
    <option value="next1">Spring 2015</option>
    <option value="next2">Summer 2015</option>
    <option value="next3">Fall 2015</option>
    */
?>
