<!DOCTYPE  html>
<html lang="en-US">

<head>
  <title>LoboPlan - TEST Results</title>
  <link href="../css/style.css" rel="stylesheet" type="text/css"> 
</head>
<body>

<?php
//Create simpleXML document
$xml = simplexml_load_file("../xml/current.xml") or die("Error: xml object not created");

$search_inst = "instructors[instructor[@primary='y'][last='Compeau']]";
$search_meet = "meeting-times[meeting-time[start-time>1000]]";

//Generate array of courses
$results = $xml->xpath("//section[@part-of-term='1H' and {$search_meet}]/..");
$count = count($results);
echo "<b>These should be courses</b> Size of results(courses): {$count}<br />";
foreach ($results as $result) {
    $type = $result->getName();
    echo "result type = {$type}<br />";
    $attrs = $result->attributes();
    foreach ($attrs as $a => $b) {
        echo "{$type} {$a} = {$b}, ";
    }
    echo "<br />";
    //children are sections
    $children = $result->children();
    foreach ($children as $child) {
        $type = $child->getName();
        echo "result type = {$type}<br />";
        $attrs = $child->attributes();
        foreach ($attrs as $a => $b) {
            echo "{$type} {$a} = {$b}, ";
        }
        echo "<br />";
        //children are section children (section-title, text, fees, credits, etc.)
        $children = $child->children();
        foreach ($children as $child) {
            $type = $child->getName();
            echo "result type = {$type}<br />";
            if ($type == 'instructors') {
                foreach ($child->children() as $instructor) {
                    $inst_attrs = $instructor->attributes();
                    $inst_child = $instructor->children();
                    if ($inst_attrs['primary'] == 'y') {
                        $first = $inst_child[0];
                        $last = $inst_child[1];
                        $email = $inst_child[3];
                        echo "Instructor: {$last}, {$first} - email: {$email}";
                    }
                }
            }
            if ($type == 'meeting-times') {
                foreach ($child->children() as $meeting_time) {
                    $meet_attrs = $meeting_time->attributes();
                    $meet_child = $meeting_time->children();
                    $start_date = $meet_child[0];
                    $end_date   = $meet_child[1];
                    $days = "";
                    foreach ($meet_child[2]->children() as $day) {
                        $days .= $day;
                    }
                    $start_time = $meet_child[3];
                    $end_time   = $meet_child[4];
                    $bldg       = $meet_child[5];
                    $bldg_code  = $bldg->attributes()['code'];
                    $room       = $meet_child[6];
                    echo "Meeting Dates: {$start_date} - {$end_date}<br />";
                    echo "Meeting Times: {$days} {$start_time} - {$end_time}<br />";
                    echo "Meeting Location: {$bldg} ({$bldg_code}) - {$room}<br />";
                }
            }
            if ($type == 'crosslists') {
                //
            }
            $attrs = $child->attributes();
            foreach ($attrs as $a => $b) {
                echo "{$type} {$a} = {$b}, ";
            }
            echo "<br />";
            //$children = $child->children();
        }
        echo "<br />";
    }
    echo "<hr />";
}
//echo "<ul>";
/*
foreach($courses as $course) {
    // Prints Course Title
    echo "<h4>{$course->attributes()['title']}</h4>";
    // Prints Course Description
    $course_children = $course->children();
    $course_catalog = $course_children[0];
    echo "<p>{$course_catalog}</p>";
    //Create sections
    //$sections = $course->children();
    
    foreach ($course_children as $section){
        if (count($section->children()) > 0) { //if $section has children, its a section
            $section_attr = $section->attributes();
            $crn = $section_attr['crn'];
            
            //Section children
            $section_title        = $section->children()[0];
            $text                 = $section->children()[1];
            $instructional_method = $section->children()[2];
            $delivery_type        = $section->children()[3];
            $enrollment           = $section->children()[4];
            $enrollment_max       = $enrollment->attributes()['max'];
            $waitlist             = $section->children()[5];
            $waitlist_max         = $waitlist->attributes()['max'];
            $credits              = $section->children()[6];
            $fees                 = $section->children()[7];
            $instructors          = $section->children()[8];
            $meeting_times        = $section->children()[9];
            //Section attributes
            $part_of_term   = $section_attr['part-of-term'];
            $status         = $section_attr['status'];
            $section_number = $section_attr['number'];
            
            if ($instructional_method == '') {
                $instructional_method = 'Not Listed';
            }
            
            if ($fees == '') {
                $fees = '0';
            }

            //Print section data
            echo "<h3><b>section: {$subject_code} - {$course_number}.{$section_number}</b></h3>";
            echo "<h4>{$section_title}</h4><br />";
            echo "<h5>{$text}</h5><br />";
            echo "<b>CRN: {$crn}</b><br />";
            echo "Part of Term - {$part_of_term}<br />";
            echo "Status: {$status}<br />";
            echo "Instruction Method: {$delivery_type} - {$instructional_method}<br />";
            echo "Enrolled: {$enrollment} out of {$enrollment_max}<br />";
            echo "Waitlisted: {$waitlist} out of {$waitlist_max}<br />";
            echo "Credits: {$credits}<br />";
            echo "Fees: $ {$fees}<br />";
            
            //Print instructor data
            foreach ($instructors->children() as $instructor) {
                if ($instructor->attributes()['primary'] == 'y') {
                    $inst_children = $instructor->children();
                    $first  = $inst_children[0];
                    $last   = $inst_children[1];
                    $middle = $inst_children[2];
                    $email  = $inst_children[3];
                    echo "Instructor: {$last}, {$first}<br />";
                    echo "email: <a href='{$email}?Subject={$subject_code} - {$course_number} . {$section_number}'>{$email}</a><br />";
                }
            }
            
            //Print Meeting time data
            //echo "Meeting times: <br/ >";
            foreach ($meeting_times->children() as $meeting_time) {
                $meeting_children = $meeting_time->children();
                $start_date = $meeting_children[0];
                $end_date   = $meeting_children[1];
                $days = "";
                foreach ($meeting_children[2]->children() as $day) {
                    $days .= $day;
                }
                $start_time = $meeting_children[3];
                $end_time   = $meeting_children[4];
                $bldg       = $meeting_children[5];
                $bldg_code  = $bldg->attributes()['code'];
                $room       = $meeting_children[6];
                echo "Start Date: {$start_date}<br />";
                echo "End Date: {$end_date}<br />";
                echo "Time: {$days} {$start_time} - {$end_time}<br />";
                echo "Location: {$bldg} ({$bldg_code}) - {$room}<br />";
            }
        }
    }
}
*/
?>
</ul>
</div>
</body>
</html>
