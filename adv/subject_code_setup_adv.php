<?php 
    $subjects = $simple_current->xpath("//subject");
    $subject_array = array('code'=>'name');
    foreach ($subjects as $subject) {
        $code = $subject->attributes()['code'];
        $name = $subject->attributes()['name'];
        $subject_array[(string)$code] = (string)$name;
    }
    //remove initial code => name
    unset($subject_array['code']);
    //sort array
    ksort($subject_array); 
    //remove duplicates
    array_unique($subject_array);
    //print values into a dropdown
    foreach ($subject_array as $a => $b) {
        echo "<option value='{$a}'>{$a} ({$b})</option>";
    }
?>
