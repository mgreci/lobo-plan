<?php 
    $courses = $simple_current->xpath("//course");
    $course_array = array();
    foreach ($courses as $course) {
        $number = $course->attributes()['number'];
        array_push($course_array,(string)$number);
    }
    $courses = $simple_next1->xpath("//course");
    foreach ($courses as $course) {
        $number = $course->attributes()['number'];
        array_push($course_array,(string)$number);
    }
    //remove initial code => name
    //unset($course_array['code']);
    //sort array
    sort($course_array); 
    //remove duplicates
    $new = array_unique($course_array);
    
    //print values into a dropdown
    foreach ($new as $a) {
        echo "<option value='course[@number='{$a}']'>{$a}</option>";
    }
?>
