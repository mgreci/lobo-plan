<?php
echo "<option value='*' selected>*ANY*</option>";
$x=5;
$post = 'AM';
while ($x < 24) {
    if ($x < 10) {
        echo "<option value='0{$x}00'>{$x}:00 AM</option>";
    } else if ($x < 12) {
        echo "<option value='{$x}00'>{$x}:00 AM</option>";
    } else if ($x == 12) {
        echo "<option value='{$x}00'>{$x}:00 PM</option>";
    } else if ($x > 12) {
        $y = $x - 12;
        echo "<option value='{$x}00'>{$y}:00 PM</option>";
    }
    $x += 1;
}
/*
<option value='0100'>1:00 AM</option>
<option value='0200'>2:00 AM</option>
<option value='0300'>3:00 AM</option>
<option value='0400'>4:00 AM</option>
<option value='0500'>5:00 AM</option>
<option value='0600'>6:00 AM</option>
<option value='0700'>7:00 AM</option>
<option value='0800'>8:00 AM</option>
<option value='0900'>9:00 AM</option>
<option value='1000'>10:00 AM</option>
<option value='1100'>11:00 AM</option>
<option value='1200'>12:00 PM</option>
<option value='1300'>1:00 PM</option>
<option value='1400'>2:00 PM</option>
<option value='1500'>3:00 PM</option>
<option value='1600'>4:00 PM</option>
<option value='1700'>5:00 PM</option>
<option value='1800'>6:00 PM</option>
<option value='1900'>7:00 PM</option>
<option value='2000'>8:00 PM</option>
<option value='2100'>9:00 PM</option>
<option value='2200'>10:00 PM</option>
<option value='2300'>11:00 PM</option>
*/
?>
