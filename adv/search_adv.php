<!DOCTYPE  html>
<html lang="en-US">

<head>
  <title>LoboPlan - Advanced Search</title>
  <link href="../css/style.css" rel="stylesheet" type="text/css"> 
</head>
<body>

<div class='header'>LoboPlan - Advanced Search</div>
<div class='search'>
<?php //$_GET['search_page'] = 'adv'; ?>
<form action="display_adv.php" method=“get”>
Semester: <select name="file">
<?php require 'semester_setup_adv.php'; ?>
</select><br />
Campus: <select name='campus_code'>
<option value='*' selected>*ANY*</option>
<?php require 'campus_setup_adv.php'; ?>
</select><br />
Subject Code: <select name='subject_code'>
<option value='*' selected>*ALL*</option>
<?php require 'subject_code_setup_adv.php'; ?>
</select><br />
Course Number: <input type="text" name="course_number"><br />
Instructor (Last name only): <input type="text" name="instructor_last"><br />
Meeting Days: (MWF, TR, etc.): <input type="text" name="meet_days"><br />
Part of Term: <select name='pOt'>
<option value='*' selected>*ANY*</option>
<option value='1'>Full Term</option>
<option value='1H'>First Half</option>
<option value='2H'>Second Half</option>
</select><br />
Start Time After: <select name="start_after">
<?php require 'time_drop_down.php'; ?>
</select><br />
Start Time Before: <select name="start_before">
<?php require 'time_drop_down.php'; ?>
</select><br />
End Time Before:<select name="end_before">
<?php require 'time_drop_down.php'; ?>
</select><br />
End Time After:<select name="end_after">
<?php require 'time_drop_down.php'; ?>
</select><br />
CRN: <input type="text" name="crn"><br />
Include online courses? <input type="checkbox" name="include_online" value="T"><br />
Show cancelled sections? <input type="checkbox" name="show_cancelled" value="T"><br />
<input type="submit"><br />
</form>
</div>
</body>
</html>
