<?php
    //Find names of campuses
    $campuses_current = $simple_current->xpath("//semester/campus");
    $campuses_next1 = $simple_next1->xpath("//semester/campus");
    //check if campus exists already
    $campuses_array = array('code' => 'name');
    foreach ($campuses_current as $campus) {
        $code = $campus->attributes()['code'];
        $name = $campus->attributes()['name'];
        $campuses_array[(string)$code] = (string)$name;
    }
    foreach ($campuses_next1 as $campus) {
        $code = $campus->attributes()['code'];
        $name = $campus->attributes()['name'];
        $campuses_array[(string)$code] = (string)$name;
    }
    unset($campuses_array['code']);
    ksort($campuses_array);
    array_unique($campuses_array);
    foreach ($campuses_array as $a => $b) {
        echo "<option value='{$a}'>{$b}</option>";
    }
?>
