<!DOCTYPE  html>
<html lang="en-US">

<head>
  <title>LoboPlan - Advanced Search Results </title>
  <link href="../css/style.css" rel="stylesheet" type="text/css"> 
</head>
<body>

<ul>
<?php
//Grab $_GET variables for searching
$semester_xml =  $_GET['file'];
$campus_code  =  $_GET['campus_code'];
$subject_code =  $_GET['subject_code'];
$course_number = $_GET['course_number'];
$inst_last = $_GET['instructor_last'];
$meeting_days = $_GET['meeting_days'];
$part_of_term = $_GET['pOt'];
$search_crn = $_GET['crn'];

//Print breadcrumb header
//echo "<div class='header'>/{$semester}/{$campusCode}/{$subjectCode}/{$courseNumber}</div>";

//Create simpleXML document
$xml = simplexml_load_file("../xml/{$semester_xml}") or die("Error: xml object not created");

if ($campus_code == '*') {
    $campus_code = 'campus';
} else {
    $campus_code = "campus[@code='{$campus_code}']";
}

if ($subject_code == '*') {
    $subject_code = 'subject';
} else {
    $subject_code = "subject[@code='{$subject_code}']";
}

if ($course_number == '') {
    $course_number = 'course';
} else {
    $course_number = "course[@number='{$course_number}']";
}

$extra_search = '[section';
$attrs = 0;
if ($search_crn != '') {
    $extra_search .= "[@crn='{$search_crn}'";
    $attrs += 1;
}
if ($part_of_term != '*') {
    if ($attrs > 0) {
        $extra_search .= ' and ';
    } else {
        $extra_search .= "[";
    }
    $extra_search .= "@part-of-term='{$part_of_term}'";
    $attrs += 1;
}
if ($search_status != ''){
    if ($attrs > 0) {
        $extra_search .= ' and ';
    } else {
        $extra_search .= "[";
    }
    $extra_search .= "@status='{$search_status}']";
}
$extra_search .= "]";

/*
if ($inst_last != '') {
     $extra_search .= ''; 
}
*/

$extra_search .= "]";

echo "extra_search = {$extra_search}<br />";
//echo "campus_code = {$campus_code}, subject_code = {$subject_code}, course_code = {$course_number}";

//Generate array of courses
//$courses = $xml->xpath("//{$campus_code}/college/department/{$subject_code}/{$course_number}[section[instructors[instructor[@primary='y'][last='Compeau']]]]");
$courses = $xml->xpath("//{$campus_code}/college/department/{$subject_code}/{$course_number}{$extra_search}");

foreach($courses as $course) {
    // Prints Course Title
    echo "<h4>{$course->attributes()['title']}</h4>";
    // Prints Course Description
    $course_children = $course->children();
    $course_catalog = $course_children[0];
    echo "<p>{$course_catalog}</p>";
    //Create sections
    //$sections = $course->children();
    
    foreach ($course_children as $section){
        if (count($section->children()) > 0) { //if $section has children, its a section
            $section_attr = $section->attributes();
            $crn = $section_attr['crn'];
            
            //Section children
            $section_title        = $section->children()[0];
            $text                 = $section->children()[1];
            $instructional_method = $section->children()[2];
            $delivery_type        = $section->children()[3];
            $enrollment           = $section->children()[4];
            $enrollment_max       = $enrollment->attributes()['max'];
            $waitlist             = $section->children()[5];
            $waitlist_max         = $waitlist->attributes()['max'];
            $credits              = $section->children()[6];
            $fees                 = $section->children()[7];
            $instructors          = $section->children()[8];
            $meeting_times        = $section->children()[9];
            //Section attributes
            $part_of_term   = $section_attr['part-of-term'];
            $status         = $section_attr['status'];
            $section_number = $section_attr['number'];
            
            if ($instructional_method == '') {
                $instructional_method = 'Not Listed';
            }
            
            if ($fees == '') {
                $fees = '0';
            }

            //Print section data
            echo "<h3><b>section: {$subject_code} - {$course_number}.{$section_number}</b></h3>";
            echo "<h4>{$section_title}</h4><br />";
            echo "<h5>{$text}</h5><br />";
            echo "<b>CRN: {$crn}</b><br />";
            echo "Part of Term - {$part_of_term}<br />";
            echo "Status: {$status}<br />";
            echo "Instruction Method: {$delivery_type} - {$instructional_method}<br />";
            echo "Enrolled: {$enrollment} out of {$enrollment_max}<br />";
            echo "Waitlisted: {$waitlist} out of {$waitlist_max}<br />";
            echo "Credits: {$credits}<br />";
            echo "Fees: $ {$fees}<br />";
            
            //Print instructor data
            foreach ($instructors->children() as $instructor) {
                if ($instructor->attributes()['primary'] == 'y') {
                    $inst_children = $instructor->children();
                    $first  = $inst_children[0];
                    $last   = $inst_children[1];
                    $middle = $inst_children[2];
                    $email  = $inst_children[3];
                    echo "Instructor: {$last}, {$first}<br />";
                    echo "email: <a href='{$email}?Subject={$subject_code} - {$course_number} . {$section_number}'>{$email}</a><br />";
                }
            }
            
            //Print Meeting time data
            //echo "Meeting times: <br/ >";
            foreach ($meeting_times->children() as $meeting_time) {
                $meeting_children = $meeting_time->children();
                $start_date = $meeting_children[0];
                $end_date   = $meeting_children[1];
                $days = "";
                foreach ($meeting_children[2]->children() as $day) {
                    $days .= $day;
                }
                $start_time = $meeting_children[3];
                $end_time   = $meeting_children[4];
                $bldg       = $meeting_children[5];
                $bldg_code  = $bldg->attributes()['code'];
                $room       = $meeting_children[6];
                echo "Start Date: {$start_date}<br />";
                echo "End Date: {$end_date}<br />";
                echo "Time: {$days} {$start_time} - {$end_time}<br />";
                echo "Location: {$bldg} ({$bldg_code}) - {$room}<br />";
            }
        }
    }
}
?>
</ul>
</div>
</body>
</html>
