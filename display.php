<!DOCTYPE  html>
<html lang="en-US">
<head>
  <title>LoboPlan - Results</title>
  <link href="css/style.css" rel="stylesheet" type="text/css"/> 
</head>
<body>
    <?php
    //Grab $_GET variables for searching
    $xml_file      = $_GET['file'];
    $campus_code   = $_GET['campuscode'];
    $subject_code  = $_GET['subcode'];
    $course_number = $_GET['coursenumber'];
    //Create simpleXML document
    $xmlDoc = simplexml_load_file("xml/{$xml_file}") or die("Error: cannot create object");
    //use campus_code or online campus 'EA'
    $campus_query = "campus[@code='{$campus_code}' or @code='EA']";
    //Generate array of courses
    $sem = $xmlDoc->xpath("//semester");
    $sem_attrs = $sem[0]->attributes();
    $semester = $sem_attrs['name'];
    $courses = $xmlDoc->xpath("//{$campus_query}/college/department/subject[@code='{$subject_code}']/course[@number='{$course_number}']");
    //Deconstruction
    $xmlDoc = null;
    $campus_query = null;
    $sem = null;
    $sem_attrs = null;
    //Print breadcrumb header
    echo "<div class='header'>\r\n";
    echo "<a class='breadcrumb' href='index.php'>LoboPlan</a>/";
    echo "<a class='breadcrumb' href='campus_select.php?semester={$semester}&file={$xml_file}'>{$semester}</a>/";
    echo "<a class='breadcrumb' href='subject_select.php?semester={$semester}&file={$xml_file}&campuscode={$campus_code}&subcode={$subject_code}'>{$campus_code}</a>/";
    echo "<a class='breadcrumb' href='course_select.php?semester={$semester}&file={$xml_file}&campuscode={$campus_code}&subcode={$subject_code}'>{$subject_code}</a>/<span>{$course_number}</span>";
    echo "\r\n</div>\r\n";
    //Deconstruction
    $semester = null;
    $sem_attrs = null;
    $xml_file = null;
    $campus_code = null;
    //Gather data, print course->sections
    if (count($courses) == 0) {
        echo "<div class='no_results'>Sorry, there is no data in here yet. Check back later!</div>\r\n";
    } else {
        for ($x=0; $x<count($courses); $x++) {
            $course = $courses[$x];
            $course_children = $course->children();
            //Print course title
            if ((int)$x == 0) {
                $cour_attrs = $course->attributes();
                echo "<div class='course_header'>{$subject_code} - {$course_number}: {$cour_attrs['title']}</div>\r\n";
                $course_catalog = $course_children[0];
                //Print catalog description
                echo "<div class='catalog'>{$course_catalog}</div>\r\n";
                echo "<ul>\r\n";
                $cour_attrs = null;
                $course_catalog = null;
            }
            //List of sections
            foreach ($course_children as $section) {
                if (count($section->children()) > 0) { //therefore a section
                    //Section children
                    $section_children     = $section->children();
                    $section_title        = $section_children[0];
                    $text                 = $section_children[1];
                    $instructional_method = $section_children[2];
                    $delivery_type        = $section_children[3];
                    $enrollment           = $section_children[4];
                    $waitlist             = $section_children[5];
                    $credits              = $section_children[6];
                    $fees                 = $section_children[7];
                    $instructors          = $section_children[8];
                    $meeting_times        = $section_children[9];
                    //Section attributes
                    $section_attr   = $section->attributes();
                    $crn            = $section_attr['crn'];
                    $pot            = $section_attr['part-of-term'];
                    $status         = $section_attr['status'];
                    $section_number = $section_attr['number'];
                    //Attributes of waitlist, enrollment
                    $wait_attrs = $section_children[5]->attributes();
                    $waitlist_max   = $wait_attrs['max'];
                    $enroll_attrs = $section_children[4]->attributes();
                    $enrollment_max = $enroll_attrs['max'];
                    //Convert $pot and $status using schedule key
                    require "misc_logic.php";
                    //Print li, section or section_cancelled
                    echo "<li>"; //class='section";
                    //if ($status != 'Active') {echo "_cancelled";}
                    //echo "'>\r\n";
                    //Print section header
                    echo "<div class='section_header'>Section: {$section_number} - CRN: {$crn}</div>\r\n";
                    //Section data
                    echo "<div class='section'>";
                    if ($section_title) {echo "Section Title: {$section_title}<br />\r\n";}
                    if ($text) {echo "Additional Information: {$text}<br />\r\n";}
                    if ($status != 'Active') {
                        echo "Status: {$status}<br />\r\n";
                    } else {
                        if ($pot != 'Full Term') {echo "Part of Term - {$pot}<br />\r\n";}
                        //Print enrolled, decide whether to include percent or not
                        echo "Enrollment: {$enrollment} out of {$enrollment_max}";
                        $percent = round(100*$enrollment/$enrollment_max, 2);
                        echo " ({$percent}% Full)<br />";
                        $percent = null;
                        if ((int)$enrollment >= (int)$enrollment_max) {
                            echo "Waitlisted: {$waitlist} out of {$waitlist_max}<br />\r\n";
                        }
                        //Gather and print Meeting Time data
                        date_default_timezone_set("UTC");
                        foreach ($meeting_times->children() as $meeting_time ) {
                            $meeting_children = $meeting_time->children();
                            $start_date = $meeting_children[0];
                            $end_date   = $meeting_children[1];
                            $days = "";
                            foreach ($meeting_children[2]->children() as $day) {
                                $days .= $day;
                            }
                            $start_time = $meeting_children[3];
                            $end_time = $meeting_children[4];
                            $bldg = $meeting_children[5];
                            $bldg_attrs = $bldg->attributes();
                            $bldg_code = $bldg_attrs['code'];
                            $room = $meeting_children[6];
                            $date_start = date("M j", strtotime($start_date));
                            $date_end = date("M j", strtotime($end_date));
                            echo "Dates: {$date_start} - {$date_end}<br />\r\n";
                            echo "Time/Location: ";
                            if ((int)$start_time == (int)$end_time or $bldg_code == 'ONLINE') {
                                echo "Online";
                            } else {
                                $time_start = date("g:iA", strtotime($start_time));
                                $time_end = date("g:iA", strtotime($end_time));
                                echo "{$days} {$time_start} - {$time_end} in <a href='http://iss.unm.edu/PCD/campus-map.html'>{$bldg_code}</a>";
                                if ($room != '') {echo "-{$room}";}
                                $time_start = null;
                                $time_end = null;
                            }
                            echo "<br />\r\n";
                            //Deconstruction
                            $meeting_children = null;
                            $start_date = null;
                            $end_date = null;
                            $days = null;
                            $start_time = null;
                            $end_time = null;
                            $bldg = null;
                            $bldg_attrs = null;
                            $bldg_code = null;
                            $room = null;
                            $date_start = null;
                            $date_end = null;
                        } 
                        //Gather and print instructor data
                        echo "Instructor: ";
                        $inst_children = $instructors->children();
                        if (count($inst_children) == 0) {
                            echo "Not Listed<br />\r\n";
                        } else {
                            foreach ($inst_children as $instructor) {
                                $inst_attrs = $instructor->attributes();
                                if ($inst_attrs['primary'] == 'y' ){
                                    $inst_children = $instructor->children();
                                    $first = $inst_children[0];
                                    $last = $inst_children[1];
                                    $email = $inst_children[3];
                                    echo "<a href='mailto:{$email}?Subject={$subject_code}-{$course_number}.{$section_number}'>{$last}, {$first}</a>";
                                    echo "<br />\r\n";
                                    //Deconstruction
                                    $first = null;
                                    $last = null;
                                    $email = null;
                                }
                            }
                        }
                        //Print other info
                        echo "Delivery Type: {$delivery_type}"; 
                        if ($instructional_method != '') {echo " - {$instructional_method}";}
                        echo "<br />\r\n";
                        echo "Credits: {$credits}<br />\r\n";
                        echo "Fees: ";
                        if ($fees != '0') {
                            echo "$ {$fees}";
                        } else {
                            echo "No Additional Fees";
                        }
                        echo "<br />\r\n";
                        //Deconstruction
                        $inst_children = null;
                    }
                    //Deconstruction
                    $section_attr = null;
                    $section_children = null;
                    $crn = null;
                    $section_number = null;
                    $pOt = null;
                    $status = null;
                    $section_title = null;
                    $text = null;
                    $instruction_method = null;
                    $delivery_type = null;
                    $enrollment = null;
                    $enroll_attrs = null;
                    $enrollment_max = null;
                    $waitlist = null;
                    $wait_attrs = null;
                    $waitlist_max = null;
                    $credits = null;
                    $fees = null;   
                    //end of section
                }
                //end of if (count(children) == 0)
                //nothing should happen here
            }
            //end of loop of sections
            echo "</div>\r\n";
            echo "</li>\r\n";
        }
        //end of list of courses
        //Deconstruction
        $course_children = null;
    }
    //end of if (count(courses) == 0)
    //Deconstruciton
    $xml_file = null;
    $campus_code = null;
    $subject_code  = null;
    $course_number = null; 
    $courses = null;
    echo "</ul>\r\n";
    require "footer.php";
    ?>
</body>
</html>
