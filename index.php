<!DOCTYPE  html>
<html lang="en-US">
<head>
    <title>LoboPlan</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <meta charset="UTF-8">
    <meta name="description" content="UNM's mobile class searching application">
    <meta name="keywords" content="Matthew Greci, Jacob Nash, LoboPlan, loboplan, UNM, unm, class search, UNM class search, unm class search, schedule, UNM schedule, unm schedule, lobo, coding project, resume, plan">
    <meta name="authors" content="Matthew Greci, Jacob Nash">
</head>
<body>
    <div class='header'><span>LoboPlan</span></div>
    <ul>
    <?php
    $xml_path = "xml/";
    $xml_array = array('current.xml','next1.xml','next2.xml','next3.xml');
    //print semesters
    foreach ($xml_array as $xml_file) {
        //Create SimpleXML object
        $xml = simplexml_load_file("{$xml_path}{$xml_file}") or die("Error: cannot create object");
        //echo "after simplexml: " . number_format(memory_get_usage()) . "<br />";
        $sem = $xml->xpath("//semester");
        $sem_attrs = $sem[0]->attributes();
        $sem_name = $sem_attrs['name'];
        echo "<li class='btn'><a href='campus_select.php?file={$xml_file}'><button><span class='index'>{$sem_name}</span></button></a></li>\r\n";
        $xml_file = null;
        $xml = null;
        $sem = null;
        $sem_attrs = null;
        $sem_name = null;
    }
    //Decontruction
    $xml_path = null;
    $xml_array = null;
    echo "</ul>";
    //require "disclaimer.php";
    require "footer.php";
    ?>
</body>
</html>
