<?php
switch ($status) {
    case "A":
        $status = 'Active';
        break;
    case "C":
        $status = '<b>Cancelled</b>';
        break;
    case "I":
        $status = '<b>Inactive</b>';
        break;
    case "M":
        $status = '<b>Cancelled with Message</b>';
        break;
    case "R":
        $status = '<b>Reserved</b>';
        break;
    case "S":
        $status = '<b>Cancelled, Rescheduled</b>';
        break;
    case "T":
        $status = '<b>Cancelled, Rescheduled with Message</b>';
        break;
}
switch ($pot) {
    case "1":
        $pot = "Full Term";
        break;
    case "SSP":
        $pot = "Special Student Programs";
        break;
    case "LAW":
        $pot = "Law Semester";
        break;
    case "1H":
        $pot = "First Half";
        break;
    case "2H":
        $pot = "Second Half";
        break;
    case "3Q":
        $pot = "Three-Quarter Term";
        break;
    case "INT":
        $pot = "Late Starting Course";
        break;
    case "OL":
        $pot = "Open Learning";
        break;
    case "NF":
        $pot = "Nursing - Full Term";
        break;
    case "N1H":
        $pot = "Nursing - First Half Term";
        break;
    case "N2H":
        $pot = "Nursing - Second Half Term";
        break;
    case "M01":
        $pot = "MD Program - Block 1";
        break;
    case "M02":
        $pot = "MD Program - Block 2";
        break;
    case "M03":
        $pot = "MD Program - Block 3";
        break;
    case "M04":
        $pot = "MD Program - Block 4";
        break;
    case "M05":
        $pot = "MD Program - Block 5";
        break;
    case "M06":
        $pot = "MD Program - Block 6";
        break;
    case "M07":
        $pot = "MD Program - Block 7";
        break;
    case "M08":
        $pot = "MD Program - Block 8";
        break;
    case "M09":
        $pot = "MD Program - Block 9";
        break;
    case "M010":
        $pot = "MD Program - Block 10";
        break;
    case "M011":
        $pot = "MD Program - Block 11";
        break;
    case "M012":
        $pot = "MD Program - Block 12";
        break;
    case "M013":
        $pot = "MD Program - Block 13";
        break;
}
?>
