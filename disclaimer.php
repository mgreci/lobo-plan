<!DOCTYPE  html>
<html lang="en-US">
<head>
    <title>LoboPlan Disclaimer</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <div class='header'><a class='breadcrumb' href='index.php'>LoboPlan</a>/<span>LoboPlan Disclaimer</span></div>
    <ul>
    <li>This data is offered to UNM students for convenience only. This schedule data is updated once a day and may not reflect the most recent changes to the Schedule of Classes. Students should use my.unm.edu to register for courses. Please contact your department's scheduling administrator with questions regarding the information displayed.</li> 
    </ul> 
    <?php
    require "footer_simple.php";
    ?>
</body>
</html>
