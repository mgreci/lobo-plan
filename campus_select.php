<!DOCTYPE  html>
<html lang="en-US">
<head>
    <title>LoboPlan - Campus Select</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/> 
</head>
<body>
    <?php 
    //Grab file from GET
    $xml_file = $_GET['file']; 
    //create xml document
    $xmlDoc = simplexml_load_file("xml/{$xml_file}");
    //obtain semester name
    $sem = $xmlDoc->xpath("//semester");
    $sem_attrs = $sem[0]->attributes();
    $semester = $sem_attrs['name'];
    //obtain array of campuses
    $campuses = $xmlDoc->xpath('//campus'); 
    //Deconstruction
    $xmlDoc = null;
    $sem = null;
    $sem_attrs = null;
    //Print breadcrumb header
    echo "<div class='header'><a class='breadcrumb' href='index.php'>LoboPlan</a>/<span>{$semester}</span></div>\r\n";
    if (count($campuses) == 0) {
        echo "<div class='no_results'>Sorry, there is no data in here yet. Check back later!</div>\r\n";
    } else {
        echo "<ul>\r\n";
        $campus_array = array('code'=>'name');
        foreach($campuses as $campus) {
            $campus_attr = $campus->attributes();
            $code = $campus_attr['code']; 
            $name = $campus_attr['name'];
            $campus_array[(string)$code] = (string)$name;
            //Deconstruction
            $campus = null;
            $campus_attr = null;
            $code = null;
            $name = null;
        }
        //Sort campuses alphabetically
        unset($campus_array['code']);
        ksort($campus_array);
        //print all campuses
        foreach($campus_array as $code => $name){
            echo ("<li class='btn'><a href='subject_select.php?file={$xml_file}&campuscode={$code}'><button>{$name}</button></a></li>\r\n");
            $code = null;
            $name = null;
        } 
        echo "</ul>\r\n";
        //Deconstruction
        $campus_array = null;
    }
    $xml_file = null;
    $semester = null;
    $campuses = null;
    require "footer.php";
    ?>
</body>
</html>
